<?php

/**
 * @file
 * Main file for the autometatags module.
 */

/**
 * Implements hook_permission().
 */

function autometatags_permission() {
  return array(
    'administer autometatags' => array(
      'title' => t('Administer Auto Meta Tags'),
      'description' => t('Allows a user to configure Auto Meta Tags settings.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */

function autometatags_menu() {
  $items['admin/config/search/autometatags'] = array(
    'title' => 'Auto Meta Tags',
    'description' => 'Configure the front page meta tags and what content types to auto generate meta tags.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('autometatags_settings_form'),
    'access arguments' => array('administer autometatags'),
    'file' => 'autometatags.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_node_view().
 */

function autometatags_node_view($node, $view_mode, $langcode) {

  // only add meta description if it does not exist.
  $search_head = drupal_get_html_head();
  $search = 'meta name="description"';
  if(strstr($search_head, $search)) {
    return;
  }

  $description = '';
  $keywords = '';
  
  // only autogenerate selected content types in settings.
  module_load_include('inc', 'autometatags');
  $content_types_all = autometatags_get_content_types();
  $type_key = array_search($node->type, $content_types_all);

  // check to see if front page first
  if (drupal_is_front_page()) {
    $description = strip_tags(variable_get('autometatags_front_desc'));
    $keywords = strip_tags(variable_get('autometatags_front_keywords'));
  }
  elseif (in_array($type_key, variable_get('autometatags_auto_desc'))) {

    // check for a user supplied summary
    if ($node->body['und'][0]['safe_summary'] == '') {
      $description = strip_tags($node->body['und'][0]['safe_value']);
    }
    else {
      $description = strip_tags($node->body['und'][0]['safe_summary']);
    }

    // TODO: Autogenerate Keywords.		
  }
  
  // trim the summary down to SEO recommended length (160)
  $split = str_split($description, 160);
  $description = $split[0];
 
  // if keywords found then add them to head
  if ($description != '') {
    // build array for adding to the header
    $meta_description = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'name' => 'description',
        'content' => trim($description),
      ),
    );
    
    // call function to add meta desc to head
    drupal_add_html_head($meta_description, 'meta_description');
  }

  // if keywords found then add them to head
  if ($keywords != '') {
    // build array for adding to the header
    $meta_keywords = array(
      '#type' => 'html_tag',
      '#tag' => 'meta',
      '#attributes' => array(
        'name' => 'keywords',
        'content' => trim($keywords),
      ),
    );
    
    // call function to add meta desc to head
    drupal_add_html_head($meta_keywords, 'meta_keywords');
  }
}