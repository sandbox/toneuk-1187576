<?php
// $Id$

/**
 * @file
 * Form builder; Configure the autometatags settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */

function autometatags_settings_form($form, &$form_state) {

  module_load_include('inc', 'autometatags');

  $form['autometatags_front_desc'] = array(
    '#type' => 'textarea',
    '#title' => t('Home Page Meta Description'),
    '#default_value' => variable_get('autometatags_front_desc'),
    '#description' => t('Enter the meta tag description for the site front page.'),
    '#size' => 160,
    '#rows' => 6,
  );
  
  $form['autometatags_front_keywords'] = array(
    '#type' => 'textarea',
    '#title' => t('Home Page Meta Keywords'),
    '#default_value' => variable_get('autometatags_front_keywords'),
    '#description' => t('Enter the meta tag keywords for the site front page.'),
    '#size' => 160,
    '#rows' => 6,
  );

  $form['autometatags_auto_desc'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Auto Generate Meta Descriptions'),
    '#default_value' => variable_get('autometatags_auto_desc'),
    '#description' => t('Select the content typed to auto generate meta tag descriptions for.'),
    '#options' => autometatags_get_content_types(),
    '#multiple' => TRUE,
  );
    
  return system_settings_form($form);
}