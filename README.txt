
What Is Auto Meta Tags

This module provides an easy way of creating front page meta descriptions and keywords.
It also has the option to specify certain content types to autogenerate meta descriptions for.

Installing Auto Meta Tags:

1. Place the entirety of this directory in sites/all/modules/autometatags
2. Navigate to admin/modules and Enable Auto Meta Tags.
3. Navigate to admin/people/permissions and assign the Administer Auto Meta Tags permission.
   This is required to setup modify the settings of the Auto Meta Tags module.

How to configure Auto Meta Tags:

1. Navigate to admin/config/services/autometatags
2. Enter the list of ping services (See example list below).
3. Select the content types to ping.
4. Select whether to ping on new, updated or both.
5. Select logging options.

The development of this module was sponsored by ZeusArticles.