<?php
// $Id$

/**
 * @file
 * autometatags include file for various module functions.
 *
 * @ingroup autometatags
 */

 /**
 * Get the sites enabled content types and return them in array.
 */

function autometatags_get_content_types() {
  
  // dont start from zero as it causes problems with checkboxes.
  $content_types = array_keys(node_type_get_types());
  array_unshift($content_types, '');
  unset($content_types[0]);
  
  return $content_types;
}

/**
 * Function to ping a service.
 */